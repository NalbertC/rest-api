const express = require('express')

const router = express.Router()
// get produtos
router.get('/', (req, res, next) => {
    res.status(200).send({
        mensagem: 'Usando GET dentro da rotas de produtos'
    })
})

//insere produto
router.post('/', (req, res, next) => {
    const produto = {
        nome: req.body.nome,
        preco: req.body.preco
    }

    res.status(201).send({
        mensagem: 'Insere um produto produtos',
        produtoCriado: produto
    })
})

// get 1 produto
router.get('/:id_produto', (req, res, next) => {
    const id = req.params.id_produto

    if (id == 'especial') {
        res.status(200).send({
            mensagem: 'Você descobriu o ID especial'
        })
    } else {
        res.status(200).send({
            mensagem: 'Usando o GET de um produto exclusivo',
            id: id
        })
    }
})

//alterar um produto
router.patch('/', (req, res, next) => {
    res.status(201).send({
        mensagem: 'Usando PATCH dentro da rota de produtos'
    })
})

//deletar um produto
router.delete('/', (req, res, next) => {
    res.status(201).send({
        mensagem: 'Usando o DELETE dentro da rota produtos'
    })
})

module.exports = router
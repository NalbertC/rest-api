const express = require('express')

const router = express.Router()


// retorna todos os pedidos
router.get('/', (req, res, next) => {
    res.status(200).send({
        mensagem: 'Retorna os pedidios'
    })
})

//insere um pedido
router.post('/', (req, res, next) => {
    const pedido = {
        id_produto: req.body.id_produto,
        quantidade: req.body.quantidade
    }
    res.status(201).send({
        mensagem: 'Pedido realizado ',
        pedidoCriado: pedido
    })
})

// get 1 produto
router.get('/:id_pedido', (req, res, next) => {
    const id = req.params.id_produto

    res.status(200).send({
        mensagem: 'Detalhes do pedido',
        id: id
    })
})

//alterar um produto
router.patch('/', (req, res, next) => {
    res.status(201).send({
        mensagem: 'Pedido alterado'
    })
})

//deletar um produto
router.delete('/', (req, res, next) => {
    res.status(201).send({
        mensagem: 'Pedido cancelado'
    })
})

module.exports = router